<?php

namespace App\DataFixtures;

use App\Entity\Teacher;
use Cocur\Slugify\Slugify;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class TeacherFixtures extends Fixture
{
    private $genders = ['male', 'female'];

    public function load(ObjectManager $manager): void
    {
        $faker = Factory::create('fr_FR');
        $slug = new Slugify();
        for ($i = 1; $i <= 10; $i++) {
            $teacher = new Teacher();
            //$gender = $faker->randomElement($this->genders);
            $teacher->setFirstName($faker->firstName/*($gender)*/);
            $teacher->setLastName($faker->lastName);
            $teacher->setEmail($slug->slugify($teacher->getFirstName()) . '.' . $slug->slugify($teacher->getLastName()) . '@gmail.com');
           // $gender = $gender == 'male' ? 'm' : 'f';
            $teacher->setImage($i/*. $gender*/. '.jpg');
            $teacher->setDescription($faker->paragraph(1, true));
            $teacher->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($teacher);

        }
        $manager->flush();

    }




}
