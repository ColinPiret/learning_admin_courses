<?php

namespace App\Classes;

use App\Entity\Course;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;


class Cart {


private $session;
private $entityManager;

    public function __construct(EntityManagerInterface $entityManager,SessionInterface $session)
    {
        $this->session = $session;
        $this->entityManager = $entityManager;


    }


    public function addtocart($id)
    {
        $cart=$this->session->get('cart',[]);

        if (!empty($cart[$id])){


            return;
        }
        else{

            $cart[$id]=1;

        }

        $this->session->set('cart',$cart);


    }

    public function removeItem($id)
    {

        $cart=$this->session->get('cart',[]);

        unset($cart[$id]);

        return  $this->session->set('cart',$cart);

    }

    public function deleteCart()
    {

      return  $this->session->remove('cart');

    }

    public function get()
    {
      return  $this->session->get('cart');
    }

    public function getFull()
    {
        $panier = [];



        if ($this->get()) {
        foreach ($this->get() as $id => $quantity){
            $course_objet = $this->entityManager->getRepository(Course::class)->findOneById($id);
            if (!$course_objet){
                $this->removeItem($id);
                continue;
            }
            $panier[] = [
                'course'=>$course_objet,
                'quantity'=>$quantity
            ];
        }

    }

        return $panier;

    }






}