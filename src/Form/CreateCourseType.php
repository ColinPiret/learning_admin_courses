<?php

namespace App\Form;

use App\Entity\Course;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CreateCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name')
            ->add('smallDescription')
            ->add('fullDescription')
            ->add('duration')
            ->add('price')
            ->add('createdAt')
            ->add('isPublished')
            ->add('slug')
            ->add('image')
            ->add('schedule')
            ->add('program')
            ->add('category')
            ->add('level')
            ->add('teacher')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
