<?php

namespace App\Form;

use App\Entity\Course;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;

class EditCourseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Intitulé',
                'attr'  => [
                    "placeholder" => 'Intitulé'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('smallDescription', TextType::class, [
                'label' => 'Courte Description',
                'attr'  => [
                    "placeholder" => 'Courte Description'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('fullDescription', TextType::class, [
                'label' => 'Intitulé',
                'attr'  => [
                    "placeholder" => 'Intitulé'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>120
                    ])
                ],
                'required'=>true
            ])
            ->add('duration', NumberType::class, [
                'label' => 'Durée',
                'attr'  => [
                    "placeholder" => 'Durée'
                ],
                'constraints'=>[
                    new length([
                        'min'=>1
                    ])
                ],
                'required'=>true
            ])
            ->add('price', NumberType::class, [
                'label' => 'Prix',
                'attr'  => [
                    "placeholder" => 'Prix'
                ],
                'constraints'=>[
                    new length([
                        'min'=>0,
                    ])
                ],
                'required'=>true
            ])
           // ->add('slug')
            ->add('image')
            ->add('schedule', TextType::class, [
                'label' => 'Intitulé',
                'attr'  => [
                    "placeholder" => 'Intitulé'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            //->add('program')
            ->add('category')
            ->add('level')
            ->add('teacher')
            ->add('submit',SubmitType::class,[
                'label'=>"Enregistrer"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Course::class,
        ]);
    }
}
