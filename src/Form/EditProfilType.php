<?php

namespace App\Form;

use App\Entity\User;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditProfilType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class, [
                'label'     => 'Votre E-mail',
                'attr'      => [
                    "placeholder" => 'Votre E-Mail'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom',
                'attr'  => [
                    "placeholder" => 'Votre prénom'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom de famille',
                'attr'  => [
                    "placeholder" => 'Votre nom de famille'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('imageFile', VichImageType::class, [
                'label'     => 'Votre avatar',
                'required'  => false,
                'attr'      => [
                    "placeholder" => 'Votre image'
                ]
            ])
          //  ->add('captcha', CaptchaType::class,[
          //      'required' => true,
          //  ])
            ->add('submit',SubmitType::class,[
                'label'=>"Enregistrer"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
