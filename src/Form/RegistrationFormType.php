<?php

namespace App\Form;

use App\Entity\User;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Vich\UploaderBundle\Form\Type\VichImageType;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstName', TextType::class, [
                'label' => 'Votre prénom',
                'attr'  => [
                    "placeholder" => 'Votre prénom'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('lastName', TextType::class, [
                'label' => 'Votre nom de famille',
                'attr'  => [
                    "placeholder" => 'Votre nom de famille'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('email', EmailType::class, [
                'label'     => 'Votre E-mail',
                'attr'      => [
                    "placeholder" => 'Votre E-Mail'
                ],
                'constraints'=>[
                    new length([
                        'min'=>2,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])
            ->add('imageFile', VichImageType::class, [
                'label'     => 'Votre avatar',
                'required'  => false,
                'attr'      => [
                    "placeholder" => 'Votre image'
                ]
            ])
            ->add('plainPassword', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'invalid_message'=>'Le mot de passe et la confirmation doivent correspondre.',
                'options' => ['attr' => ['class' => 'mot de passe']],
                'required' => true,
                'first_options' => [
                    'label' =>'Mot de passe',
                    'attr' => ['placeholder' => 'Mot de passe'
                    ]],
                'second_options' => [
                    'label' => 'Repetez mot de passe',
                    'attr' => ['placeholder' => 'confirmez mot de passe'
                    ]],
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password']
                

            ])
            ->add('agreeTerms', CheckboxType::class, [
                'label' => 'Accepter conditions générales',
                'mapped' => false,
                'constraints' => [
                    new IsTrue([
                        'message' => 'Vous devez accepter les conditions',
                    ]),
                ],
            ])
            ->add('captcha', CaptchaType::class,[
                'required' => true,
            ])
            ->add('submit',SubmitType::class,[
                'label'=>"S'inscrire"
            ])
        ;

    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
