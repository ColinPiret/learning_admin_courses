<?php

namespace App\Form;

use App\Entity\News;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Vich\UploaderBundle\Form\Type\VichImageType;

class EditNewType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'Intitulé',
                'attr'  => [
                    "placeholder" => 'Intitulé'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>60
                    ])
                ],
                'required'=>true
            ])

            ->add('content', TextType::class, [
                'label' => 'News',
                'attr'  => [
                    "placeholder" => 'news'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>200
                    ])
                ],
                'required'=>true
            ])
            ->add('image', VichImageType::class, [
                'label'     => 'Image',
                'required'  => false,
                'attr'      => [
                    "placeholder" => 'Image'
                ]
            ])
            //->add('slug')
            ->add('author')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => News::class,
        ]);
    }
}
