<?php

namespace App\Form;

use App\Entity\User;
use Gregwar\CaptchaBundle\Type\CaptchaType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class EditPasswordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('email', EmailType::class,[
                'disabled' => true
            ])

            ->add('firstName',TextType::class,[
                'disabled' => true
            ])
            ->add('lastName',TextType::class,[
                'disabled' => true
            ])
            ->add('old_password', PasswordType::class,[
                'label'=>'mot de passe actuel',
                'attr'=>[
                    'placeholder'=>'veuillez saisir votre nouveau mot de passe actuel'
                ],
                'mapped' => false,
                'required' => true,
            ])
            ->add('new_password', RepeatedType::class, [
                // instead of being set onto the object directly,
                // this is read and encoded in the controller
                'type' => PasswordType::class,
                'invalid_message'=>'Le mot de passe et la confirmation doivent correspondre.',
                'options' => ['attr' => ['class' => 'mot de passe']],
                'required' => true,
                'first_options' => [
                    'label' =>'Nouveau Mot de passe',
                    'attr' => ['placeholder' => 'Entrez votre nouveau mot de passe'
                    ]],
                'second_options' => [
                    'label' => 'Repetez nouveau mot de passe',
                    'attr' => ['placeholder' => 'Confirmez votre nouveau mot de passe'
                    ]],
                'mapped' => false,
                'attr' => ['autocomplete' => 'new-password']
            ])

            ->add('captcha', CaptchaType::class,[
                'required' => true,
            ])
            ->add('submit',SubmitType::class,[
                'label'=>"Enregistrer"
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
