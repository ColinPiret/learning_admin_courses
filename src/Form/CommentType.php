<?php

namespace App\Form;

use App\Entity\Comment;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;

class CommentType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'Titre du commentaire',
                'attr'  => [
                    "placeholder" => 'Titre du commentaire'
                ],
                'constraints'=>[
                    new length([
                        'min'=>5,
                        'max'=>120
                    ])
                ]
            ])
            ->add('rating', IntegerType::class, [
                'label' => 'Votre note',
                'constraints'=>[
                    new length([
                        'min'=>1,
                        'max'=>5
                    ])
                ],
                'attr'=>[
                    'min'=>'1',
                    'max'=>'5'
                ],
            ])
            ->add('comment', TextareaType::class, [
                'label' => 'Votre commentaire',
                'constraints'=>[
                    new NotBlank(['message'=>'Ecrivez votre commentaire'])
                ]

            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Comment::class,
        ]);
    }
}
