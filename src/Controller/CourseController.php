<?php

namespace App\Controller;

use App\Entity\Comment;
use App\Entity\Course;
use App\Form\CommentType;
use App\Form\CreateCourseType;
use App\Form\EditCourseType;
use App\Repository\CommentRepository;
use App\Repository\CourseCategoryRepository;
use App\Repository\CourseLevelRepository;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CourseController extends AbstractController
{
    /**
     * @param CourseCategoryRepository $categoryRepository
     * @param CourseRepository $courseRepository
     * @return Response
     */
    #[Route('/courses', name: 'courses')]
    public function courses(CourseCategoryRepository $categoryRepository,CourseLevelRepository $levelRepository, CourseRepository $courseRepository): Response
    {
        $levels = $levelRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $categories = $categoryRepository->findBy(
            [],
            ['name' => 'ASC']
        );
        $courses = $courseRepository->findBy(
            ['isPublished' => true],
            ['name' => 'ASC']
        );
        return $this->render('course/courses.html.twig', [
            'categories' => $categories,
            'levels'=>$levels,
            'courses' => $courses
        ]);
    }

    /**
     * @param Course $course
     * @return Response
     */
    #[Route('/course/{slug}', name: 'course')]
public function course(Course $course, CommentRepository $commentRepository, Request $request, EntityManagerInterface $manager): Response
    {
        $comment = new Comment();
        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $comment->setAuthor($this->getUser());
            $comment->setCourse($course);
            $comment->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($comment);
            $manager->flush();
            // Création du message flash
            $this->addFlash(
                'success',
                'Votre commentaire a bien été posté!'
            );
        }

        //$comments = $course->getComments().ob_get_length();


        //$noteAvg = array_sum($comments)/$comments

        return $this->renderForm('course/course.html.twig', [
            'course' => $course,
            'form' => $form,
            //'noteavg'=>$noteAvg
        ]);


    }









}
