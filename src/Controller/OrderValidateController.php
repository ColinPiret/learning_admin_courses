<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderValidateController extends AbstractController
{
    private $entityManager;
    public function __construct(EntityManagerInterface $entityManager){
        $this->entityManager=$entityManager;
    }

    #[Route('/commande/success/{stripeSessionId}', name: 'order_validate')]
    public function index(Cart $cart,$stripeSessionId): Response
    {
        $order = $this->entityManager->getRepository(Order::class)->findOneByStripeSessionId($stripeSessionId);

        if (!$order || $order->getUser() != $this->getUser()){

            return $this->redirectToRoute('home');
        }

        if (!$order->getIsPaid()){


            $cart->deleteCart();

            $order->setIsPaid(1);
            $this->entityManager->flush();


        }


        return $this->render('order_validate/index.html.twig', [
            'order'=>$order
        ]);
    }
}
