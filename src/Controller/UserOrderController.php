<?php

namespace App\Controller;

use App\Entity\Order;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserOrderController extends AbstractController
{

    #[Route('/profile/mes-commandes', name: 'account_order')]
    public function index(OrderRepository $orderRepository): Response
    {
        $orders= $orderRepository->findSuccessOrders($this->getUser());

        return $this->render('user/order.html.twig', [
            'orders' => $orders,
        ]);
    }

    #[Route('/profile/mes-commandes/{reference}', name: 'account_order_info')]
    public function orderInfo(OrderRepository $orderRepository,$reference): Response
    {
        $order= $orderRepository->findOneByReference($reference);

        if (!$order || $order->getUser() != $this->getUser())
        {
            return $this->redirectToRoute('account_order');

        }

        return $this->render('user/orderInfo.html.twig', [
            'order' => $order,
        ]);
    }




}
