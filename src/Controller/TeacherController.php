<?php

namespace App\Controller;

use App\Entity\Teacher;
use App\Entity\User;
use App\Repository\CourseRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeacherController extends AbstractController
{
    #[Route('/teacher', name: 'teacher')]
    public function index(TeacherRepository $teacherRepository, CourseRepository $courseRepository ): Response
    {

        $teachers = $teacherRepository->findAll();
        $courses = $courseRepository->findAll();

        return $this->render('teacher/teacher.html.twig', [
            'teachers'=>$teachers,'courses'=>$courses
        ]);
    }

    #[Route('/teacher/{id}', name: 'teacher_info')]
    public function teacherInfo(Teacher $teacher, CourseRepository $courseRepository): Response
    {

        $courses = $courseRepository->findAll();
        return $this->render('teacher/teacherInfo.html.twig', [
            'teacher'=>$teacher,'courses'=>$courses
        ]);
    }

}
