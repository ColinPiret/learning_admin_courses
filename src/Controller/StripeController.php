<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Course;
use App\Entity\Order;
use Doctrine\ORM\EntityManagerInterface;
use Stripe\Checkout\Session;
use Stripe\Stripe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class StripeController extends AbstractController
{
    #[Route('/commande/create-session/{reference}', name: 'stripe_create_session')]
    public function index(EntityManagerInterface $entityManager,Cart $cart, $reference): Response
    {
        $courses_for_stripe = [];
        $YOUR_DOMAIN = 'http://127.0.0.1:8000';

        $order = $entityManager->getRepository(Order::class)->findOneByReference($reference);

        if (!$order){
            new JsonResponse(['error'=>'order']);
        }

        foreach ($order->getOrderDetails()->getValues() as $course){
            $course_object= $entityManager->getRepository(Course::class)->findOneByName($course->getCourse());
            $courses_for_stripe[] = [
                'price_data'=>[
                    'currency'=>'eur',
                    'unit_amount'=>$course->getPrice()*100,
                    'product_data'=> [
                        'name'=>$course->getCourse(),
                      'images'=>[$YOUR_DOMAIN.'/img/sections/'.$course_object->getImage()]
                    ],
                ],
                'quantity' => $course->getQuantity(),

            ];
        }

        Stripe::setApiKey('sk_test_51KHjbrBssLTArLuW61p2THSjroPXzRZ1rRAbya3IXZOsJeGhMKUvkhh43p9h5eoXny4hjpli5d0nOrRYzTDUSUka00ePRQATEB');


        $checkout_session = Session::create([
            'customer_email'=>$this->getUser()->getEmail(),
            'payment_method_types'=>['card'],
            'line_items' => [
                $courses_for_stripe
            ],
            'mode' => 'payment',
            'success_url' => $YOUR_DOMAIN . '/commande/success/{CHECKOUT_SESSION_ID}',
            'cancel_url' => $YOUR_DOMAIN . '/commande/error/{CHECKOUT_SESSION_ID}',
        ]);

        $order->setStripeSessionId($checkout_session->id);

        $entityManager->flush();

        $response = new JsonResponse(['id'=>$checkout_session->id]);

        return $response;
    }



}
