<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\EditPasswordType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;

class UserPasswordController extends AbstractController
{
    #[Route('profile/edit-password', name: 'edit_password')]
    public function editPassword(UserPasswordHasherInterface $userPasswordHasher, Request $request): Response
    {
      $user = $this->getUser();
            $form = $this->createForm(EditPasswordType::class, $user);

            $form->handleRequest($request);

           if ($form->isSubmitted() && $form->isValid() ) {

               $oldPassword = $form->get('old_password')->getData();


               if ($userPasswordHasher->isPasswordValid($user,$oldPassword)){

                   //$newPassword = $form->get('new_password')->getData();

                   // encode the new_password
                   $user->setPassword(
                       $userPasswordHasher->hashPassword(
                           $user,
                           $form->get('new_password')->getData()
                       )
                   );

                   $entityManager = $this->getDoctrine()->getManager();
                   //$entityManager->persist($user); //persist putôt pour la creation d'entité
                   $entityManager->flush();
                   $this->addFlash(
                       'success',
                       'Votre mot de passe a bien été mis à jour!'
                   );

                return $this->redirectToRoute('profil');
               }
               else{
                   $this->addFlash(
                       'warning',
                       "Votre mot de passe actuel n'est pas le bon. Retapez le "
                   );
               }


            }

           /////

            return $this->render('user/edit-password.html.twig', [
                'editPasswordType' => $form->createView(),
            ]);


    }



}
