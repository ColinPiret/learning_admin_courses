<?php

namespace App\Controller;

use App\Classes\Cart;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeaderController extends AbstractController
{
    #[Route('/header', name: 'header')]
    public function index(Cart $cart): Response
    {
        return $this->render('partials/header.html.twig', [
            'panier'=>$cart->getFull()
        ]);
    }
}
