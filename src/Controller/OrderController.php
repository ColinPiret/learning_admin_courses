<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Order;
use App\Entity\OrderDetails;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class OrderController extends AbstractController
{

    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
       $this->entityManager= $entityManager;
    }

    #[Route('/commande', name: 'order')]
    public function index(Cart $cart): Response
    {
        return $this->render('order/order.html.twig', [
            'controller_name' => 'OrderController',
            'panier'=>$cart->getFull()
        ]);
    }

    #[Route('/commande/recapitulatif', name: 'order_recap')]
    public function add(Cart $cart): Response
    {
        //enregistrer la commande
        $order = new Order();
        $reference = (new \DateTimeImmutable())->format('dmy').'-'.uniqid();
        $order->setReference($reference);
        $order->setUser($this->getUser());
        $order->setCreatedAt(new \DateTimeImmutable());
        $order->setIsPaid(0);
        $this->entityManager->persist($order);

        //enregistrer les cours orderDetails
        foreach ($cart->getFull() as $course){
            $orderDetails = new OrderDetails();
            $orderDetails->setMyOrder($order);
            $orderDetails->setCourse($course['course']->getName());
            $orderDetails->setQuantity($course['quantity']);
            $orderDetails->setPrice($course['course']->getPrice());
            $orderDetails->setTotal($course['course']->getPrice()*$course['quantity']);
            $this->entityManager->persist($orderDetails);


        }

       $this->entityManager->flush();

        return $this->render('order/addOrder.html.twig', [
            'panier'=>$cart->getFull(),
            'reference'=>$order->getReference()
          ]);
    }















}
