<?php

namespace App\Controller;

use App\Repository\CourseRepository;
use App\Repository\NewsRepository;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name: 'home')]
    public function index(NewsRepository $newsRepository, CourseRepository $courseRepository, Request $request): Response
    {


        $news = $newsRepository->findBy(array(),
            array('id' => 'DESC'),
            4,
            0);
        $courses= $courseRepository->coursesHomePage();


        return $this->render('home/index.html.twig',[
            'courses'=>$courses,'news'=>$news
        ]);


    }

  /*
  public function lastestNews(EntityManager $entityManager): Response
    {
       $dql = 'SELECT news FROM AppBundle\Entity\news news, DATE_DIFF(CURRENT_DATE(), news.createdAt) as days WHERE days <= 30 ORDER BY news.name DESC';

       $query = $entityManager->createQuery($dql);
       return $query->execute();

    }
  */


}
