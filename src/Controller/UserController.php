<?php

namespace App\Controller;

use App\Form\EditProfilType;
use App\Repository\CommentRepository;
use App\Repository\CourseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends AbstractController
{
    #[Route('/profile/view', name: 'profil')]
    public function profil(CommentRepository $commentRepository, CourseRepository $courseRepository): Response
    {
        $comments = $commentRepository->findAll();
        $courses = $courseRepository->findAll();

        return $this->render('user/profil.html.twig',
        [
            'comments'=>$comments,
            'courses'=>$courses
        ]);
    }

    #[Route('/profile/edit-profil', name: 'edit_profil')]
    public function editProfil(Request $request): Response
    {

        $user = $this->getUser();

        $form = $this->createForm(EditProfilType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            if(empty($user->getImageFile())) $user->setImage('default.jpg');
            $user->setUpdatedAt(new \DateTimeImmutable());
           // $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash(
                'success',
                'Votre profil a bien été mis à jour!'
            );
            return $this->redirectToRoute('home');
        }

        return $this->render('user/edit-profil.html.twig',[
            'editProfilForm' => $form->createView(),
        ]);
    }




}
