<?php

namespace App\Controller;

use App\Classes\Cart;
use App\Entity\Course;
use App\Repository\CourseRepository;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{



#[Route('/panier', name: 'cart_index')]
    public function index(Cart $cart,CourseRepository $courseRepository): Response
    {
/*
      $panier = [];
      foreach ($cart->get() as $id => $quantity){

          $panier[] = [

              'course'=>$courseRepository->findOneById($id),
              'quantity'=>$quantity
          ];
      }
*/
        return $this->render('cart/panier.html.twig',[
            'panier'=>$cart->getFull()
        ]);


    }



#[Route('/panier/add/{id}', name: 'cart_add')]
    public function addtocart(Cart $cart,$id): Response
    {


        $cart->addtocart($id);

        $this->addFlash(
            'success',
            'Ce cours a bien été ajouté à votre panier!'
        );
       return $this->redirectToRoute('courses');
    }

#[Route('/panier/remove/{id}', name: 'cart_removeItem')]
    public function removeItem(Cart $cart,$id): Response
    {

        $cart->removeItem($id);

       return $this->redirectToRoute('cart_index');
    }

    #[Route('/panier/delete', name: 'cart_delete')]
    public function deleteCart(Cart $cart): Response
    {

        $cart->deleteCart();

       return $this->redirectToRoute('courses');
    }

















    /*


    private $requestStack;
    private $session;
    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
        $this->session = $this->requestStack->getSession();
    }

    #[Route('/panier', name: 'cart_index')]
    public function index(CourseRepository $courseRepository): Response
    {

        $panier = $this->session->get('panier',[]);

        $panierWithData = [];
        foreach ($panier as $id =>$quantity){

$course=$courseRepository->find($id);
            $panierWithData[] = [
                'product'=> $courseRepository->find($id),
                'quantity'=>$quantity,
                //'image'=>$course->getImage(),
                //'category'=>$course->getCategory(),
                'price'=>$course->getPrice(),
                //'level'=>$course->getLevel(),
                //'schedule'=>$course->getSchedule(),
                //'description'=>$course->getSmallDescription(),
            ];
        }

        $total = 0;
        foreach ($panierWithData as $item){

            $totalItem = $item['product']->getPrice()*$item['quantity'];

            $total += $totalItem;
        }


        //dd($this->session->get('panier'));
        return $this->render('cart/panier.html.twig', [
            'items'=>$panierWithData
        ]);
    }


    #[Route('/panier/add/{id}', name: 'cart_add')]
    public function addtocart($id): Response
    {
           $panier = $this->session->get('panier',[]);

           if(!empty($panier[$id])){

               $panier[$id]++;
           }
           else{
               $panier[$id]=1;
           }


        $panier = $this->session->set('panier',$panier);
        dd($this->session->get('panier'));

     //   return $this->render('cart/panier.html.twig', [
       //    'controller_name' => 'CartController',
       // ]);
    }

*/


}
