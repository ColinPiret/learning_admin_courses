<?php

namespace App\Controller;

use App\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TeamController extends AbstractController
{
    #[Route('/teacher', name: 'teacher')]
    public function team(UserRepository $userRepository): Response
    {
        $team = $userRepository->findByRoles();

        return $this->render('teacher/order.html.twig', [
         'teacher'=>$team
        ]);
    }
}
