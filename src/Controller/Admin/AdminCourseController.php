<?php

namespace App\Controller\Admin;

use App\Entity\Comment;
use App\Entity\Course;
use App\Entity\News;
use App\Entity\User;
use App\Form\CreateCourseType;
use App\Form\EditCourseType;
use App\Form\EditNewType;
use App\Form\EditProfilType;
use App\Form\EditUserType;
use App\Repository\CommentRepository;
use App\Repository\CourseRepository;
use App\Repository\NewsRepository;
use App\Repository\TeacherRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AdminCourseController extends AbstractController
{
    /**
     * @param CourseRepository $repository
     * @return Response
     */
    #[Route('/admin/courses', name: 'admin_courses')]
    public function adminCourses(CourseRepository $repository): Response
    {
        $courses = $repository->findBy(
           [],
           ['createdAt' => 'DESC']
    );
        return $this->render('admin/courses/courses.html.twig',[
            'courses' => $courses,
        ]);
    }

    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/viewcourse/{id}', name: 'admin_view_course')]
    public function viewCourse(Course $course, EntityManagerInterface $manager): Response
    {
        $course->setIsPublished(!$course->getIsPublished());
        $manager->flush();
        return $this->redirectToRoute('admin_courses');
    }



    /**
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param $slug
     * @return Response
     */
    #[Route('/admin/editcourse/{id}', name: 'admin_edit_course')]
    public function editCourse(Request $request, EntityManagerInterface $entityManager,$id): Response
    {
        $course = $entityManager->getRepository(Course::class)->find($id);

        $form = $this->createForm(EditCourseType::class, $course);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {

            $course->setAuthor($this->getUser());
            $course->setCourse($course);
            $course->setCreatedAt(new \DateTimeImmutable());
            //$entityManager->persist($course);
            $entityManager->flush();
            // Création du message flash
            $this->addFlash(
                'success',
                'Ce cours a bien été mis a jour!'
            );

            return $this->redirectToRoute('admin_courses');
        }

        return $this->renderForm('admin/courses/editCourse.html.twig', [
            'editCourseForm' => $form->createView()

        ]);


    }

    /**
     * @param Request $request
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/createcourse/', name: 'admin_create_course')]
    public function createCourse(Request $request, EntityManagerInterface $manager, TeacherRepository $teacherRepository): Response
    {
        $course = new Course();

        $form = $this->createForm(CreateCourseType::class, $course);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {

            $course->setCreatedAt(new \DateTimeImmutable());
            $manager->persist($course);
            $manager->flush();
            // Création du message flash
            $this->addFlash(
                'success',
                'Ce cours a bien été créé!'
            );
        }

        return $this->redirectToRoute('admin_courses');


    }


    /**
     * @param Course $course
     * @param EntityManagerInterface $manager
     * @return Response
     */
    #[Route('/admin/delcourse/{id}', name: 'admin_del_course')]
public function delCourse(Course $course, EntityManagerInterface $manager) :response {
        $manager->remove($course);
        $manager->flush();
        $this->addFlash(
            'success',
            'Le cours '.$course->getName(). ' a bien été supprimé'
        );
        return $this->redirectToRoute('admin_courses');
    }


    #[Route('/admin/comments', name: 'admin-comments')]
    public function comments(CommentRepository $commentRepository): Response
    {
        $comments = $commentRepository->findAll();
        return $this->render('admin/comments/comments.html.twig',
            [
                'comments'=>$comments,

            ]);
    }


/**
 * @param Comment $comment
 * @param EntityManagerInterface $manager
 * @return Response
 */
    #[Route('/admin/delcomment/{id}', name: 'admin_del_comment')]
public function delComment(Comment $comment, EntityManagerInterface $manager) :response {
        $manager->remove($comment);
        $manager->flush();
        $this->addFlash(
            'success',
            'Ce cours commentaire a bien été supprimé'
        );
        return $this->redirectToRoute('admin-comments');
    }



    #[Route('/admin/news', name: 'admin-news')]
    public function news(NewsRepository $newsRepository): Response
    {
        $news = $newsRepository->findAll();
        return $this->render('admin/news/news.html.twig',
            [
                'news'=>$news,

            ]);
    }

    /**
     * @param EntityManagerInterface $entityManager
     * @param Request $request
     * @param $id
     * @return Response
     */
    #[Route('/admin/news/{id}', name: 'admin_edit-news')]
    public function editNew(EntityManagerInterface $entityManager,Request $request,$id): Response
    {

        $new = $entityManager->getRepository(News::class)->find($id);

        $form = $this->createForm(EditNewType::class, $new);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            if(empty($new->getImageFile())) $new->setImage('defaultNew.png');
            $new->setUpdatedAt(new \DateTimeImmutable());
            // $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash(
                'success',
                'New mis à jour!'
            );
            return $this->redirectToRoute('admin-news');
        }

        return $this->render('admin/news/editNew.html.twig',[
            'editNewForm' => $form->createView(),
        ]);
    }



    #[Route('/admin/delnew/{id}', name: 'admin_del_new')]
    public function delNews(News $new, EntityManagerInterface $manager) :response {
        $manager->remove($new);
        $manager->flush();
        $this->addFlash(
            'success',
            'Cette news a bien été supprimée'
        );
        return $this->redirectToRoute('admin_courses');
    }

    #[Route('/admin/admins', name: 'admin-admins')]
    public function admins(UserRepository $userRepository): Response
    {
        $admins = $userRepository->findAll();
        return $this->render('admin/admins/admins.html.twig',
            [
                'admins'=>$admins,

            ]);
    }

    #[Route('/admin/users', name: 'admin-users')]
    public function users(UserRepository $userRepository): Response
    {
        $users = $userRepository->findAll();
        return $this->render('admin/users/users.html.twig',
            [
                'users'=>$users,

            ]);
    }

    #[Route('/admin/users/{id}', name: 'admin_edit-user')]
    public function editProfil(EntityManagerInterface $entityManager,Request $request,$id): Response
    {

        $user = $entityManager->getRepository(User::class)->find($id);

        $form = $this->createForm(EditUserType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $entityManager = $this->getDoctrine()->getManager();
            if(empty($user->getImageFile())) $user->setImage('default.jpg');
            $user->setUpdatedAt(new \DateTimeImmutable());
            // $entityManager->persist($user);
            $entityManager->flush();
            // do anything else you need here, like send an email
            $this->addFlash(
                'success',
                'Profil mis à jour!'
            );
            return $this->redirectToRoute('admin-users');
        }

        return $this->render('user/edit-profil.html.twig',[
            'editProfilForm' => $form->createView(),
        ]);
    }


    #[Route('/admin/deluser/{id}', name: 'admin_del_user')]
    public function delUser(User $user, EntityManagerInterface $manager) :response {
        $manager->remove($user);
        $manager->flush();
        $this->addFlash(
            'success',
            'Cet utilisateur a bien été supprimé'
        );
        return $this->redirectToRoute('admin-users');
    }











}


