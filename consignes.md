## USERS
Via le dropdown:
 1) Affichez le profil du user (ex: card...) et ses propres commentaires    ////fait
 2) Permettre la modification du profil                                     ////corriger upload image
 3) Permettre la modification du password (*)                               ////fait
 
 Captcha pour le registration form (*)                                      ////fait

## COMMENTS
1) Ajoutez la moyenne des notations pour les commentaires.  N'effectuez le calcul que si le cours possède au moins deux commentaires   

2) Lors de l'insertion d'un commentaire validez les champs:
    1) Title (5 car min et 120 car max)                                    /////fait
    2) Rating (pas en dessous de 1 et pas supérieur à 5)                   /////fait
    3) Comment (notBlank)                                                   /////fait
    
   Un utilisateur ne peut poster qu'un seul commentaire par cours (*)      /////fait
    
## NEWS
Sur base de l'entité crée en classe affichez via la navbar l'ensemble des news ne dépassant pas 1 mois d'ancienneté.      /////fait creer new pour être sur

## HOMEPAGE
Affichez les trois derniers cours et les quatre dernières actualités          ///fait creer une news et cours pour être sûr

## TEACHERS
Affichez sur une vue les profs et leurs cours                                

## STATIC PAGE
About: page ergonomique et esthétique

## TEAM
Affichez des cards avec les admins et les super admins


                                                      