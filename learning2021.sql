-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : ven. 14 jan. 2022 à 04:20
-- Version du serveur :  5.7.31
-- Version de PHP : 8.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `learning2021`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `rating` int(11) NOT NULL,
  `comment` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CF675F31B` (`author_id`),
  KEY `IDX_9474526C591CC992` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`id`, `author_id`, `course_id`, `created_at`, `rating`, `comment`, `title`) VALUES
(81, 135, 63, '2021-10-29 12:02:35', 3, 'Ipsum quisquam ratione delectus est neque error. Soluta dolores dignissimos consequatur dolores ut et qui alias. Harum non eos consequatur hic iure fugiat minima. Recusandae sit saepe praesentium non voluptate quas. Placeat omnis tempore mollitia ut.', 'necessitatibus maiores optio'),
(82, 150, 58, '2021-10-29 12:02:35', 3, 'Et cumque iusto atque alias consectetur asperiores. Architecto aut mollitia dolorem quibusdam. Corrupti qui repellat in est ut excepturi et.', 'enim sint quod'),
(83, 112, 75, '2021-10-29 12:02:35', 5, 'Ipsa deserunt sapiente quia adipisci id laudantium. Debitis neque quia provident aut aut. Illum quaerat soluta est harum qui. Illo totam inventore quibusdam inventore. Earum repellendus ut consequuntur voluptatem vitae dolorem.', 'ad nostrum eius'),
(84, 149, 59, '2021-10-29 12:02:35', 5, 'Deserunt repellendus rerum cumque explicabo doloribus esse sint. Assumenda minus iusto beatae numquam voluptas.', 'ut est placeat'),
(85, 124, 75, '2021-10-29 12:02:35', 3, 'Placeat rerum at odio reiciendis. Ut eos laborum eius voluptatum. Reiciendis eius et unde repellendus. Nesciunt sed unde ipsum dicta sed. Officiis aspernatur doloremque fuga distinctio.', 'provident in soluta'),
(86, 127, 75, '2021-10-29 12:02:35', 1, 'Expedita ex saepe quos ut cupiditate voluptatem. Enim blanditiis id non quasi qui ipsa maiores deserunt. Illum sapiente id necessitatibus est totam deleniti.', 'voluptatem amet et'),
(87, 117, 67, '2021-10-29 12:02:35', 5, 'Assumenda ratione est et repellat beatae voluptas dolor beatae. Aut non beatae voluptas id excepturi. Eligendi autem aut vel voluptatem odio quaerat. Facilis sed commodi sunt quia officiis.', 'ut excepturi beatae'),
(88, 133, 71, '2021-10-29 12:02:35', 5, 'Et dolor ad ipsa adipisci. Aut est modi omnis et ad consequatur autem. Eligendi minus non distinctio ducimus. Expedita voluptas ipsum incidunt.', 'cum accusantium laboriosam'),
(89, 141, 54, '2021-10-29 12:02:35', 3, 'Odit et sit veritatis vero. Praesentium ipsum corporis et consequatur consequatur eligendi aut. Ut molestias ad voluptatem.', 'et et ab'),
(90, 134, 57, '2021-10-29 12:02:35', 1, 'Sequi possimus cum porro. Aliquam repudiandae est deserunt nobis quis doloribus dolor ex. Qui sapiente distinctio rerum voluptatem.', 'quam accusamus dolor'),
(91, 114, 73, '2021-10-29 12:02:35', 5, 'Unde est velit et tenetur. Voluptatem eos qui et corporis. Vitae molestiae dolorem impedit. Fugit saepe qui illo qui.', 'et nisi dolor'),
(92, 107, 71, '2021-10-29 12:02:35', 5, 'Ex ut sint excepturi. Sit consequatur architecto ut quasi nihil et. Et iure quas amet.', 'consequatur atque molestiae'),
(93, 107, 78, '2021-10-29 12:02:35', 5, 'Dolor soluta ut in est vel est ea enim. Amet minus impedit voluptatem quia suscipit voluptas iusto. Facere aliquid saepe consequatur omnis eveniet sed.', 'possimus animi assumenda'),
(94, 131, 53, '2021-10-29 12:02:35', 1, 'Qui sit molestiae inventore dolorem voluptate ipsam. Doloribus aliquid aperiam delectus qui. Excepturi sit quas voluptate dolorum.', 'soluta debitis dignissimos'),
(95, 143, 74, '2021-10-29 12:02:35', 5, 'Ea sed consequatur est sed vel inventore est. Omnis delectus iusto rerum et eum ut. A ut ipsam quia illo illum aut quidem delectus.', 'voluptatem dolore eligendi'),
(96, 119, 60, '2021-10-29 12:02:35', 3, 'Excepturi officiis corrupti recusandae corporis qui dolore. Quae aperiam laboriosam non voluptatem quae est mollitia. Quia praesentium debitis dolore.', 'minima quo atque'),
(97, 142, 77, '2021-10-29 12:02:35', 1, 'Sit doloribus odit sit vitae saepe accusantium. Voluptate nihil dignissimos dolores veritatis natus. Quidem sed alias aliquam voluptatem quia odio molestias. Aut quos facilis fugit et.', 'possimus ab dolorem'),
(98, 137, 74, '2021-10-29 12:02:35', 2, 'Ratione consequatur provident dolore quasi id. Accusantium accusantium totam voluptas consequatur et et tenetur. Voluptatem autem voluptas nihil eaque earum et aperiam. Dolorem repellendus quia debitis delectus vel aut.', 'ut laborum saepe'),
(99, 136, 67, '2021-10-29 12:02:35', 1, 'Eius quia est consectetur quas illum aliquam. Repellendus sequi omnis officiis perferendis amet maxime. Doloribus sint quisquam nisi et. Quisquam et quis ipsam consectetur dolores eius rerum et. Voluptas officia fuga aperiam incidunt.', 'quaerat dolorem in'),
(100, 144, 55, '2021-10-29 12:02:35', 4, 'Nulla aspernatur cupiditate consectetur et. Sapiente ea et culpa. Labore dolorum doloremque quis dolorem aliquid.', 'rerum nostrum est'),
(101, 113, 76, '2021-10-29 12:02:35', 5, 'Minima voluptas excepturi eaque sit provident. Ducimus eaque quam est recusandae sint. Molestiae sed sed aut blanditiis minima. Qui maiores quod nulla architecto nemo commodi.', 'et sed voluptatibus'),
(102, 120, 78, '2021-10-29 12:02:35', 4, 'Repellendus non culpa velit quaerat. Et repudiandae ratione repudiandae quisquam molestias excepturi quia quas. Consequuntur dolorem consectetur vero quis ad non hic. Placeat voluptate voluptas corrupti repudiandae. Voluptatibus voluptate natus a possimus.', 'vel culpa nihil'),
(103, 108, 54, '2021-10-29 12:02:35', 1, 'Qui delectus quo perspiciatis corporis modi alias. Cumque non et quia. Commodi error sequi tempora dignissimos saepe ut. Reprehenderit voluptas et dolores autem assumenda.', 'mollitia et qui'),
(104, 120, 74, '2021-10-29 12:02:35', 1, 'Impedit accusamus neque quis veritatis. Voluptatum architecto quo a accusantium. Ut minus quia qui tenetur. Aut dolor id sit esse non quas perspiciatis iure.', 'consequatur animi voluptas'),
(105, 141, 58, '2021-10-29 12:02:35', 4, 'Aut id tenetur tempora ea quibusdam. Sunt voluptatem voluptatem id laboriosam molestiae vitae numquam. Ipsam aspernatur aperiam qui voluptate repellat. Ab temporibus aut sed aliquid quisquam dolorum.', 'pariatur ducimus non'),
(106, 140, 70, '2021-10-29 12:02:35', 2, 'Voluptatem ut consequatur sunt numquam et animi aliquam. Tenetur assumenda dolores rem expedita ea. Mollitia libero mollitia quas vel sed aut. Exercitationem ut molestiae et modi dolor suscipit eos.', 'minus possimus ut'),
(107, 146, 63, '2021-10-29 12:02:35', 4, 'Rerum quas sit reprehenderit corporis sed saepe. Voluptates nulla saepe nobis ratione. Fugiat sit maiores ipsum.', 'ab autem eos'),
(108, 136, 68, '2021-10-29 12:02:35', 5, 'Reprehenderit sapiente voluptate voluptas nobis qui cupiditate. Voluptatem voluptatem cupiditate ea voluptas. Porro molestias voluptatem maiores quis ratione nobis omnis.', 'impedit et et'),
(109, 127, 77, '2021-10-29 12:02:35', 3, 'Accusamus laboriosam sapiente eaque qui doloremque quis. Nihil fugiat natus veritatis ut.', 'et sed dolorem'),
(110, 133, 73, '2021-10-29 12:02:35', 5, 'Quo voluptatibus rem explicabo voluptatem voluptatem. Quis esse et dolorem id. Quos esse quasi explicabo omnis. Occaecati itaque maxime eveniet odio.', 'ut sed consequatur'),
(111, 134, 56, '2021-10-29 12:02:35', 3, 'Repellendus magnam neque dolor qui nihil. Perferendis voluptas placeat impedit et debitis dolor rem. Sunt tenetur quia natus dicta nulla.', 'ut aut error'),
(112, 132, 60, '2021-10-29 12:02:35', 1, 'Magni tempore velit reprehenderit expedita et consequuntur. Ut quis qui corrupti iusto alias ut pariatur. Sed neque delectus quo doloribus velit. Quisquam perspiciatis doloremque deserunt dolorum magni enim.', 'adipisci non rerum'),
(113, 115, 74, '2021-10-29 12:02:35', 3, 'Alias quo et qui ut eaque nobis error. Amet et sint placeat repellendus molestiae eum et. Quasi eaque minima soluta perferendis sunt sunt accusamus.', 'possimus rerum assumenda'),
(114, 154, 57, '2021-10-29 12:02:35', 4, 'Consequuntur tempora illum occaecati quae magni. Corrupti eius quia voluptatum soluta. Dignissimos sapiente est voluptas vel autem. Reiciendis rem molestiae quasi labore nemo quis debitis.', 'nemo reiciendis quas'),
(115, 121, 60, '2021-10-29 12:02:35', 1, 'Tempore vel error eos sapiente nam quas. Ea qui iure id fuga qui neque voluptatum.', 'iure corrupti et'),
(116, 156, 73, '2021-10-29 12:02:35', 1, 'Quam aut et quia. Repudiandae veritatis eum sed dolores. Ea illo saepe qui ut eligendi nam error.', 'id in sunt'),
(117, 123, 53, '2021-10-29 12:02:35', 2, 'Maiores possimus magni voluptas assumenda iusto corporis porro. Et debitis quo iusto error quos temporibus esse est. Et et doloribus et.', 'omnis unde et'),
(118, 108, 67, '2021-10-29 12:02:35', 1, 'Ea ut eligendi et laudantium tempore ut. Dolores quod error et quia a adipisci voluptates neque. Commodi assumenda perspiciatis repellat. Autem asperiores accusamus laudantium similique pariatur.', 'in suscipit est'),
(119, 155, 64, '2021-10-29 12:02:35', 1, 'Esse natus qui ab dolorem qui. Consequatur quia incidunt eveniet quasi. Dolorem voluptatem vero ex rerum rerum aut.', 'quos voluptates numquam'),
(120, 106, 65, '2021-10-29 12:02:35', 4, 'Consequatur molestiae accusamus ipsa est doloribus aut. Amet asperiores quos optio culpa.', 'sit non vero');

-- --------------------------------------------------------

--
-- Structure de la table `course`
--

DROP TABLE IF EXISTS `course`;
CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `level_id` int(11) NOT NULL,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `small_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_description` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` int(11) NOT NULL,
  `price` double NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `program` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_169E6FB912469DE2` (`category_id`),
  KEY `IDX_169E6FB95FB14BA7` (`level_id`)
) ENGINE=InnoDB AUTO_INCREMENT=79 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course`
--

INSERT INTO `course` (`id`, `category_id`, `level_id`, `name`, `small_description`, `full_description`, `duration`, `price`, `created_at`, `is_published`, `slug`, `image`, `schedule`, `program`) VALUES
(53, 17, 12, 'Perspiciatis et.', 'Quia magnam autem doloremque iusto pariatur. Natus modi sint vel.', 'Asperiores error reiciendis perspiciatis omnis occaecati. Voluptatum sint neque ut quae ut. Libero non rerum tempora tenetur in nobis voluptatem. Saepe in sunt ut est non debitis. Est porro ducimus neque facilis. Pariatur est asperiores qui.', 60, 150, '2021-10-29 12:02:34', 0, 'perspiciatis-et', '1.jpg', 'Thursday', '1.pdf'),
(54, 18, 9, 'Officiis sunt et.', 'Asperiores nihil dolorem impedit aliquid numquam.', 'Beatae neque impedit dolores praesentium nulla. Iste consequatur voluptates quasi quod tempore assumenda vero praesentium. Harum enim sint nemo excepturi. Aperiam saepe et reprehenderit dolore nisi enim voluptatem. Maiores consequatur dolorum eveniet ratione magni. Et eius unde quod autem. Nisi dolorem enim suscipit aliquam.', 180, 250, '2021-10-29 12:02:34', 1, 'officiis-sunt-et', '2.jpg', 'Monday', '2.pdf'),
(55, 15, 9, 'Quis velit.', 'Porro facilis aut dolores aut sed minima.', 'Omnis aspernatur officia aliquam voluptatem consequatur. Rem velit itaque non voluptas excepturi doloribus. Soluta impedit accusantium dolor voluptatem. Ut sunt nisi quasi id. Dicta impedit deserunt fugiat rem. Totam molestias voluptatem tenetur et ipsam sit.', 120, 80, '2021-10-29 12:02:34', 0, 'quis-velit', '3.jpg', 'Sunday', '3.pdf'),
(56, 21, 9, 'Voluptatem dolorum.', 'Explicabo architecto velit vel veniam ut sunt quo. Voluptas omnis voluptatibus delectus est soluta omnis sint.', 'Eum at nulla sit nobis minus enim ipsam. Rerum delectus totam libero possimus nostrum sapiente. Accusamus minus molestiae vero qui. Ad inventore sed consectetur. Aliquid iure laudantium enim. Fugiat pariatur quisquam culpa tempora soluta eum. Itaque esse possimus accusantium consequatur rem.', 360, 250, '2021-10-29 12:02:34', 1, 'voluptatem-dolorum', '4.jpg', 'Thursday', '4.pdf'),
(57, 17, 11, 'Dignissimos id non.', 'Laudantium accusamus earum eum et et odio rerum.', 'Natus quod veritatis animi necessitatibus. Et ipsum fuga officiis et. Quaerat non enim cumque quod in ducimus eius nihil. Repellendus dolore veritatis facere repellat non sint impedit.', 120, 400, '2021-10-29 12:02:34', 1, 'dignissimos-id-non', '5.jpg', 'Sunday', '5.pdf'),
(58, 16, 12, 'Sit commodi rerum.', 'Vel neque unde aut nostrum.', 'Est aut nobis voluptas rerum animi omnis consequuntur. Qui et omnis inventore quis cumque id. Animi quam reprehenderit a unde consequatur. Quia libero assumenda nesciunt fugit rerum officia.', 180, 80, '2021-10-29 12:02:34', 1, 'sit-commodi-rerum', '6.jpg', 'Sunday', '6.pdf'),
(59, 17, 10, 'Necessitatibus velit.', 'Est aut voluptas eum id.', 'Et amet maiores reiciendis doloremque inventore vero ut voluptatum. Excepturi omnis consequatur voluptatem. Qui nobis dicta iste in adipisci vel tempore. Illum et est debitis ut. Facere numquam quas voluptatum nobis error debitis.', 120, 300, '2021-10-29 12:02:34', 1, 'necessitatibus-velit', '7.jpg', 'Friday', '7.pdf'),
(60, 21, 12, 'Consequatur iste in.', 'Optio necessitatibus animi occaecati minus doloremque qui et.', 'Facilis iste explicabo qui deleniti. Repellendus quia quia qui nostrum. Ullam qui atque sint. Exercitationem amet doloribus ex. Sed consequatur qui consequatur soluta. Rem pariatur reprehenderit qui illum.', 600, 200, '2021-10-29 12:02:34', 1, 'consequatur-iste-in', '8.jpg', 'Sunday', '8.pdf'),
(61, 16, 10, 'Alias est.', 'Molestias et et doloribus aut sint vitae odit.', 'Et enim eligendi est pariatur. Excepturi doloribus fuga deleniti. Aut voluptate aliquid sit. Modi repellendus voluptatum autem dignissimos qui. Animi sit natus et delectus hic ut ducimus. Et fugiat incidunt impedit dolorem. Esse deserunt quas cupiditate sit.', 600, 400, '2021-10-29 12:02:34', 1, 'alias-est', '9.jpg', 'Monday', '9.pdf'),
(62, 15, 10, 'Tenetur quis.', 'Odio aliquam corporis possimus.', 'Accusamus corrupti et possimus minus molestiae. Inventore id eius quasi nobis repellat dolorum id. Id eveniet nulla ratione perferendis minima. Maiores temporibus voluptatem alias consequatur laudantium debitis. Exercitationem atque possimus voluptas est id adipisci laborum. Harum est hic excepturi hic sit occaecati fugiat. Rem exercitationem tempora quia voluptatem.', 60, 150, '2021-10-29 12:02:34', 1, 'tenetur-quis', '10.jpg', 'Thursday', '10.pdf'),
(63, 15, 9, 'Magni ipsam autem.', 'Commodi est iure quia accusamus deleniti ut fuga quo.', 'Minus possimus nostrum ut. Aut minus commodi sequi quaerat voluptatem soluta ipsam eos. A quaerat iusto dolor omnis. Similique ratione officiis eligendi illo ad sed. Qui laudantium mollitia accusantium magnam placeat.', 120, 150, '2021-10-29 12:02:34', 1, 'magni-ipsam-autem', '11.jpg', 'Thursday', '11.pdf'),
(64, 21, 10, 'Et sint aut.', 'Eum occaecati dolorem debitis vel quasi.', 'Culpa ut voluptatibus eum soluta. Esse et dolor quam a. Quia neque vel et natus. Numquam sunt nam accusamus quas possimus at. Laboriosam optio asperiores natus officia rem autem. Sapiente qui ut neque.', 200, 340.5, '2021-10-29 12:02:34', 1, 'et-sint-aut', '12.jpg', 'Friday', '12.pdf'),
(65, 20, 11, 'Sed aut.', 'Aut voluptatem saepe eos exercitationem. Eum veritatis sapiente in nobis repellendus beatae.', 'Cupiditate ratione est sed libero beatae perspiciatis id suscipit. Sint alias quam perspiciatis quisquam quia perspiciatis. Voluptas omnis ex magni aliquid. Consequatur facilis occaecati tenetur nostrum dicta odit. Quae commodi aliquid consectetur laudantium id aliquid. Non id sint officiis id sapiente eius unde.', 120, 80, '2021-10-29 12:02:34', 1, 'sed-aut', '13.jpg', 'Monday', '13.pdf'),
(66, 17, 11, 'Vel est.', 'Alias dolorem vero doloribus fugiat. Nihil quia exercitationem consequuntur laborum.', 'Ea dolores iure non et. Corrupti vel ratione nihil et iure voluptate error. Accusamus minima non ut dignissimos. Quo repellat vero omnis impedit ut.', 120, 250, '2021-10-29 12:02:34', 1, 'vel-est', '14.jpg', 'Wednesday', '14.pdf'),
(67, 18, 9, 'Corrupti eaque recusandae.', 'Et sit et error aut. Ex consequatur culpa tenetur qui.', 'Eaque rerum quos deleniti adipisci. Aut dolores incidunt illo. Quibusdam officiis fuga maiores dolorem reiciendis. In quidem hic ut. Debitis ipsam aut et inventore est voluptate recusandae doloremque. Dolorem aut mollitia ut qui.', 60, 300, '2021-10-29 12:02:34', 1, 'corrupti-eaque-recusandae', '15.jpg', 'Saturday', '15.pdf'),
(68, 18, 12, 'Et ut.', 'Quia sit magnam ratione possimus quam nulla. Dolore cumque architecto repellendus similique velit repellendus.', 'Eos doloremque tempore nemo. Vel voluptatem architecto quod fugiat facilis ex molestias doloribus. Qui praesentium minus est voluptatum aut ea quae. Sed sequi occaecati ut qui aut. Minus et incidunt quo quia hic ullam sunt.', 500, 400, '2021-10-29 12:02:34', 1, 'et-ut', '16.jpg', 'Tuesday', '16.pdf'),
(69, 20, 9, 'Tempore totam sequi.', 'Sunt numquam omnis voluptas et temporibus velit. Non sunt laboriosam ipsa repudiandae.', 'Sequi non voluptatem omnis quisquam delectus omnis ea sequi. Quidem atque earum odio alias fugit iste. Consequatur ipsam veritatis ipsa necessitatibus repellendus. Fugiat vitae aspernatur hic iste.', 600, 200, '2021-10-29 12:02:34', 1, 'tempore-totam-sequi', '17.jpg', 'Sunday', '17.pdf'),
(70, 20, 12, 'Sunt quidem.', 'Et incidunt deleniti rerum est quos delectus voluptatem. Optio earum itaque est eligendi minus nam nulla laborum.', 'Laudantium rerum a eum omnis laudantium iure delectus. Et recusandae iure voluptate eius. Et incidunt dolorem omnis. Deserunt cupiditate adipisci et commodi autem mollitia dolorem.', 200, 80, '2021-10-29 12:02:34', 1, 'sunt-quidem', '18.jpg', 'Saturday', '18.pdf'),
(71, 16, 12, 'Unde quia modi.', 'Repellat aut voluptas et maiores aliquam commodi quisquam. Alias at dolorem aut sit dolorem.', 'Atque voluptatem quam corrupti officiis voluptas in. Ipsam dolorem eaque a ea sint nisi. Facere eaque voluptatum eaque. Laboriosam velit in ullam odio.', 300, 250, '2021-10-29 12:02:34', 1, 'unde-quia-modi', '19.jpg', 'Saturday', '19.pdf'),
(72, 19, 10, 'Fugit nobis doloribus.', 'Commodi beatae sint occaecati occaecati et.', 'Et omnis amet alias omnis ut quasi. Nam voluptatem unde accusamus. Aliquam repudiandae ipsa qui exercitationem eos commodi non. Corrupti saepe est ullam ea. Voluptas est quam maiores perspiciatis dolor dolore. Inventore iure sint explicabo rem repellat est. Non eaque sunt nemo.', 60, 300, '2021-10-29 12:02:34', 1, 'fugit-nobis-doloribus', '20.jpg', 'Wednesday', '20.pdf'),
(73, 15, 11, 'Quidem voluptas.', 'Rem ratione ut earum voluptates. Dicta ullam explicabo illo.', 'Aliquam vel et ut labore quia omnis. Nobis qui quo porro iusto possimus occaecati. Dolorem culpa sit et quam et quibusdam ut eligendi. Ad fugit ut esse cumque voluptates nihil possimus eum. Omnis maxime labore minus et et sint.', 600, 200, '2021-10-29 12:02:34', 1, 'quidem-voluptas', '21.jpg', 'Friday', '21.pdf'),
(74, 20, 10, 'Ullam ut.', 'Exercitationem ex id ea maxime voluptates accusantium placeat.', 'Alias voluptatum illum illum eum. Assumenda aut non voluptas. Optio veniam sapiente rerum consequuntur quas atque unde. Est non quos voluptatem porro enim officiis asperiores.', 60, 400, '2021-10-29 12:02:34', 0, 'ullam-ut', '22.jpg', 'Monday', '22.pdf'),
(75, 19, 12, 'Odio sed.', 'Dolore et voluptas sapiente a.', 'Consequatur officiis iure doloremque perferendis quo in. Voluptatum similique ratione est minus minima quia ab. Consectetur numquam repudiandae non cumque facere tempora aliquid. Excepturi illo aspernatur magni molestias tempora ea repellendus.', 600, 400, '2021-10-29 12:02:34', 1, 'odio-sed', '23.jpg', 'Thursday', '23.pdf'),
(76, 20, 12, 'Veritatis in aut.', 'Distinctio ea earum quidem.', 'Et aut dolor culpa reiciendis. Tenetur suscipit deleniti architecto qui rem harum nihil. Accusamus officiis id esse. Officiis consequuntur quam accusamus neque voluptas quis aperiam. Vitae quo et est totam incidunt cumque. Doloremque quod nemo laboriosam repellat hic suscipit.', 120, 120, '2021-10-29 12:02:34', 1, 'veritatis-in-aut', '24.jpg', 'Saturday', '24.pdf'),
(77, 20, 9, 'Eius et.', 'Consequatur et iusto iure rerum. Impedit deserunt deleniti quaerat voluptates.', 'Error consequuntur rerum quia officia quam culpa. Doloremque mollitia nulla dolor exercitationem autem. Deserunt blanditiis quasi aut consequatur ducimus ab dolores. Eaque totam ex eveniet voluptas labore. Voluptatem odio rerum quo autem aut modi.', 60, 150, '2021-10-29 12:02:34', 1, 'eius-et', '25.jpg', 'Thursday', '25.pdf'),
(78, 15, 10, 'Impedit totam.', 'Et nostrum ullam voluptatibus ex ut vel.', 'Quidem aut debitis aperiam. Adipisci et rem tenetur et veritatis placeat ratione. Architecto ut aut ea eius adipisci optio voluptas. Expedita quam porro iure mollitia eos doloribus at.', 600, 340.5, '2021-10-29 12:02:34', 1, 'impedit-totam', '26.jpg', 'Sunday', '26.pdf');

-- --------------------------------------------------------

--
-- Structure de la table `course_category`
--

DROP TABLE IF EXISTS `course_category`;
CREATE TABLE IF NOT EXISTS `course_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course_category`
--

INSERT INTO `course_category` (`id`, `name`, `description`) VALUES
(15, 'Artisanat', 'Aut expedita in consequatur voluptas.'),
(16, 'Bien être', 'Ut vero in consequatur voluptas sint.'),
(17, 'Webdeveloper', 'Dolor ex accusamus ut.'),
(18, 'Langues', 'Tempore accusantium enim quae alias vel.'),
(19, 'Technique', 'Recusandae cumque voluptatem deleniti fugiat at beatae.'),
(20, 'Informatique', 'Voluptates eum dolores ad omnis eius ullam.'),
(21, 'Pédagogique', 'Ratione eos quas ut ea expedita.');

-- --------------------------------------------------------

--
-- Structure de la table `course_level`
--

DROP TABLE IF EXISTS `course_level`;
CREATE TABLE IF NOT EXISTS `course_level` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prerequisite` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `course_level`
--

INSERT INTO `course_level` (`id`, `name`, `prerequisite`) VALUES
(9, 'Débutant', 'Certificat de base'),
(10, 'Confirmé', 'Connaissances de base'),
(11, 'Spécialisé', 'Connaissances avancées'),
(12, 'Expert', 'Pratique professionnelle et expertise');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20211029072538', '2021-10-29 10:07:08', 4589),
('DoctrineMigrations\\Version20211029100523', '2021-10-29 10:07:13', 1037);

-- --------------------------------------------------------

--
-- Structure de la table `news`
--

DROP TABLE IF EXISTS `news`;
CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_published` tinyint(1) NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1DD39950F675F31B` (`author_id`)
) ENGINE=InnoDB AUTO_INCREMENT=105 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `news`
--

INSERT INTO `news` (`id`, `author_id`, `name`, `created_at`, `updated_at`, `content`, `image`, `is_published`, `slug`) VALUES
(55, 150, 'Omnis.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Eum sunt earum corporis veniam sed debitis saepe. Sunt incidunt pariatur illo debitis commodi. Ut eum ut ut ipsam et veritatis suscipit. Dicta aut ut adipisci laborum est aut maiores.', '1.png', 1, 'omnis'),
(56, 110, 'Atque.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Corporis excepturi porro autem fugiat totam ut. Et modi culpa qui. Quidem qui ipsam soluta eaque est quis non non. Et et sed quia omnis. Sit perferendis perspiciatis nulla quia alias.', '2.png', 0, 'atque'),
(57, 117, 'Veniam.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Eligendi harum velit voluptatem et. Voluptatibus accusamus itaque illum placeat placeat. Autem id eum ipsam ipsa. Suscipit consequatur et iusto. Qui enim enim recusandae similique odit.', '3.png', 1, 'veniam'),
(58, 138, 'Iure quas.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Quia laudantium aut consequatur optio quos est maxime. Enim sunt et sit quos error ducimus. Aliquam rerum molestiae in illum aspernatur.', '4.png', 0, 'iure-quas'),
(59, 111, 'Animi aut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Nisi cum eos quis qui. Aliquam in qui quis sequi eius. Dolor ea voluptatum nesciunt asperiores sed rerum. Inventore neque tempora veniam quod. Repellat omnis quia odio minima sed nihil architecto.', '5.png', 0, 'animi-aut'),
(60, 124, 'Maiores.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Voluptas incidunt facilis sint quisquam nihil qui quia. Ipsum sequi dignissimos error praesentium quod. Voluptatem nihil animi labore repellendus aspernatur.', '6.png', 1, 'maiores'),
(61, 136, 'Quia.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Excepturi excepturi sed perferendis consectetur. Assumenda perspiciatis accusamus animi dolores est dignissimos ipsum. Et exercitationem facilis quia dicta. Eum dolor quam magnam provident.', '7.png', 0, 'quia'),
(62, 134, 'Et maxime.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Exercitationem iure omnis dolorum aperiam eum quidem. Quis molestias unde ut sed nihil. Sapiente voluptates aperiam explicabo suscipit atque ut dolor. Deserunt ex vel excepturi enim adipisci saepe.', '8.png', 0, 'et-maxime'),
(63, 138, 'Tempore.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Eveniet soluta quasi eaque quidem ducimus veniam. Quam est quisquam necessitatibus beatae fuga enim sit. Eos aliquid explicabo maxime. Laborum iusto quod maiores occaecati est.', '9.png', 0, 'tempore'),
(64, 128, 'Molestiae.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Consequatur qui molestias placeat nobis voluptas. Quis animi non in dignissimos. Non reiciendis ut et possimus commodi consequatur.', '10.png', 0, 'molestiae'),
(65, 153, 'Eum.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Debitis non exercitationem facilis natus dolorem sapiente corporis. Vel magnam est exercitationem.', '11.png', 1, 'eum'),
(66, 154, 'Id aut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Sit quas magnam veritatis. Voluptatem quaerat nulla ea architecto. In dolore esse ut et odio qui. Possimus rerum consequatur explicabo quo consequatur.', '12.png', 1, 'id-aut'),
(67, 136, 'Nihil.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Dicta amet quod sequi officiis cumque. Et nihil a dignissimos sint et quia ut. Ut quod laborum ullam aut officia aut iusto. Perferendis autem illum enim neque labore.', '13.png', 1, 'nihil'),
(68, 123, 'Et est.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Non molestiae voluptatibus soluta amet. Aperiam illo fugiat et impedit. Iure voluptatem nam reprehenderit ut et magni quibusdam.', '14.png', 1, 'et-est'),
(69, 154, 'Quidem.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Velit laudantium sit eius autem adipisci mollitia cupiditate. Rerum quo cumque minus hic unde corrupti. Et autem deleniti eos ut voluptatem.', '15.png', 1, 'quidem'),
(70, 109, 'Sequi aut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Labore modi aut ex natus aspernatur aut qui aliquid. Quos rerum fugiat odio sunt voluptas numquam corrupti. Dolorem est sit beatae consequatur. Omnis nulla perspiciatis quia aut cumque.', '16.png', 1, 'sequi-aut'),
(71, 143, 'Ut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Nihil esse ipsum qui maiores ducimus. Eum nisi fugit sint dolorem tempore consequatur. Iste modi culpa dolor qui hic.', '17.png', 0, 'ut'),
(72, 146, 'Possimus.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Asperiores mollitia et ab consectetur quaerat. Voluptas autem error saepe alias ab. Officia quibusdam minus et et nihil eos inventore. Qui doloremque odit voluptate dolore.', '18.png', 0, 'possimus'),
(73, 154, 'Velit est.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Dignissimos quisquam rem sit. Cum accusamus reiciendis nobis ducimus consequuntur incidunt aut neque. A minima ut rem.', '19.png', 0, 'velit-est'),
(74, 153, 'Quos quo.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Iusto qui eaque pariatur quidem quis alias in. Temporibus autem sunt optio libero. Reiciendis dolorum voluptas animi accusamus voluptatem.', '20.png', 1, 'quos-quo'),
(75, 149, 'Animi.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Aut provident dolor ut. Esse et eum officia veritatis velit molestiae et. Sed itaque voluptates recusandae ducimus. Laborum soluta voluptatem rem est aut qui.', '21.png', 1, 'animi'),
(76, 122, 'Aut nemo.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Labore illum aspernatur hic est sit vero fugiat. Voluptatem est rerum voluptatibus ipsam voluptatem. Qui assumenda aut est autem. Qui aut est aspernatur tempora aut nulla et.', '22.png', 0, 'aut-nemo'),
(77, 155, 'Cumque.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Corrupti sit voluptates quia nihil. Porro sint qui ipsum ratione quo libero molestiae. Suscipit porro labore eaque doloremque rerum.', '23.png', 0, 'cumque'),
(78, 126, 'Aut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Sequi quibusdam vel dolorem voluptatem. Natus quia nihil molestiae molestiae soluta inventore at facilis. Perferendis consectetur qui animi earum numquam dolorem architecto. Debitis et rerum eum consequatur voluptatem iusto animi.', '24.png', 1, 'aut'),
(79, 151, 'Dolorem.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Earum inventore vel fugit sapiente quam. Porro quis ipsam vitae. Iusto autem odio sunt molestias ullam iure numquam molestias. Est ex assumenda rerum dolorum sed accusantium.', '25.png', 0, 'dolorem'),
(80, 147, 'Facilis.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Pariatur ea commodi enim est velit. Nisi vitae aut ea id dolor. Ut animi consequatur sed est explicabo. Nemo quisquam sequi commodi aut.', '26.png', 0, 'facilis'),
(81, 145, 'Delectus.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Voluptatem omnis vel qui a ut vero temporibus dolorem. Quam molestiae cum aut iste ut sequi. Delectus ipsum dolores aut illo.', '27.png', 0, 'delectus'),
(82, 133, 'Autem sit.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Consequuntur sunt et illo sed occaecati sed. Voluptatibus facere sed animi sit qui nemo quo. Saepe pariatur vero necessitatibus tempore rerum harum mollitia ducimus. Atque quidem a magni esse tempora qui adipisci. Debitis aliquam et ut iure.', '28.png', 1, 'autem-sit'),
(83, 128, 'Molestias.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Alias iure suscipit alias ut. Neque numquam deserunt iste illum aut itaque. Quo voluptatem a ratione et omnis nulla. Est aliquam inventore hic. Sunt laborum fugiat corporis.', '29.png', 0, 'molestias'),
(84, 155, 'Nemo et.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Vel numquam quod vel voluptates. Dignissimos laborum blanditiis quo ex. Aut et voluptas voluptas. Beatae quisquam blanditiis est in.', '30.png', 0, 'nemo-et'),
(85, 112, 'Ex ut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Quas voluptas labore aut praesentium in. Saepe perspiciatis est rerum labore in nisi.', '31.png', 1, 'ex-ut'),
(86, 130, 'Aut saepe.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Inventore qui adipisci eius et libero deleniti. Molestiae consequuntur voluptatem illum quia et aut est iste. Eius dolor non recusandae. Consectetur qui alias officia ut sunt voluptate quo. Expedita ipsam quis officiis esse reprehenderit non.', '32.png', 0, 'aut-saepe'),
(87, 116, 'Minus qui.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Doloribus inventore dicta earum tempore. Asperiores commodi voluptatem a fugit iure dolorem officiis eaque.', '33.png', 1, 'minus-qui'),
(88, 149, 'Assumenda.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Odit illo quia omnis quaerat et explicabo et. Corporis adipisci pariatur qui sed. A sit praesentium et voluptatem ea.', '34.png', 0, 'assumenda'),
(89, 105, 'Sunt.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'In iure et magnam repellendus. Et ipsa amet et temporibus natus modi dicta. Sunt minima cupiditate labore.', '35.png', 1, 'sunt'),
(90, 150, 'Et id qui.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Explicabo similique aut vero vel fugit soluta et. Corrupti non aperiam laudantium excepturi sit architecto dolorem.', '36.png', 0, 'et-id-qui'),
(91, 126, 'Doloribus.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'At eos repudiandae sint vero. Maiores et vero rerum sit esse in repudiandae. Inventore aspernatur sint harum qui facilis aut. Error aliquid excepturi explicabo.', '37.png', 0, 'doloribus'),
(92, 134, 'In non.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Sunt labore autem veniam mollitia est. Suscipit ut ut minus consequatur commodi. Eaque in et quia autem velit.', '38.png', 0, 'in-non'),
(93, 151, 'Eligendi.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Minus quis optio aperiam illo corrupti reprehenderit asperiores. Voluptas commodi est est repellendus omnis est. Mollitia ut voluptatibus recusandae autem aliquid est.', '39.png', 0, 'eligendi'),
(94, 111, 'Quo.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Harum nostrum unde voluptatem occaecati id. In repellendus deleniti dolorum quia quidem ullam. Veniam delectus est dolor eum est ut.', '40.png', 0, 'quo'),
(95, 113, 'Et.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Sed aliquid odio cum. Rem fugit neque voluptatibus laboriosam esse saepe fuga. Aut maxime eos nam possimus commodi voluptatibus.', '41.png', 1, 'et'),
(96, 127, 'Veritatis.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Ipsam est amet eos tenetur omnis facilis ipsum. Aliquam quo ullam sequi harum sapiente reiciendis. Nihil ut saepe laborum qui alias ratione distinctio. Minima sapiente quibusdam vitae et omnis eum. Excepturi corrupti atque at ut aut eius vero sit.', '42.png', 1, 'veritatis'),
(97, 117, 'Eum.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Et enim et perspiciatis officia vero veritatis. Consectetur dolores libero et. Consequatur perspiciatis tenetur nam fuga voluptas eum necessitatibus voluptatem. Omnis aut est doloribus est laudantium natus.', '43.png', 1, 'eum'),
(98, 144, 'Qui quasi.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Reprehenderit ut dolores voluptatem. Nobis debitis sint nulla aliquam omnis natus. Suscipit reiciendis aut voluptas fugit. Aliquid illum est asperiores quasi. Cumque quo doloremque minima.', '44.png', 1, 'qui-quasi'),
(99, 127, 'Aliquam.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Tempore necessitatibus labore fuga et qui necessitatibus. Quia fugiat placeat qui sed et. Magni rem maxime et. Ad necessitatibus veniam doloremque autem et.', '45.png', 0, 'aliquam'),
(100, 154, 'Sed aut.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Et corrupti magni eius. Blanditiis dolores velit eos consequatur quos soluta placeat. Blanditiis ea repellendus accusantium blanditiis magni maxime.', '46.png', 1, 'sed-aut'),
(101, 144, 'Porro.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Non quia sed modi corrupti. Dolorum delectus corporis excepturi sequi ipsam tenetur. Voluptatem nesciunt iste modi et in molestiae.', '47.png', 1, 'porro'),
(102, 139, 'Non.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Praesentium minima nesciunt magni. Tenetur modi atque accusamus saepe molestiae harum nobis. Quam tempore commodi quod est praesentium omnis.', '48.png', 1, 'non'),
(103, 132, 'Excepturi.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Ipsum exercitationem non ut exercitationem. Exercitationem dolor quis aperiam labore. Recusandae ut quas cupiditate amet officia.', '49.png', 0, 'excepturi'),
(104, 115, 'Quisquam.', '2021-10-29 12:02:35', '2021-10-29 12:02:35', 'Nihil quod sunt consequatur animi ut temporibus nisi. Voluptatem numquam est voluptatem consequatur ad occaecati non id. Commodi nisi asperiores voluptate magni voluptas quia itaque nobis.', '50.png', 1, 'quisquam');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `roles` json NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(120) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `updated_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `last_log_at` datetime NOT NULL COMMENT '(DC2Type:datetime_immutable)',
  `is_disabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_8D93D649E7927C74` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=157 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `email`, `roles`, `password`, `first_name`, `last_name`, `image`, `created_at`, `updated_at`, `last_log_at`, `is_disabled`) VALUES
(105, 'gilles.berthelot@gmail.com', '[\"ROLE_USER\"]', '$2y$13$BfM24RrQm0GROMSs7Br99eIiyhUYn2UiBO.TwBNT5HxL8sJEriZ.2', 'Gille', 'Berthelot', 'default.jpg', '2021-10-29 12:02:06', '2022-01-04 04:40:58', '2021-10-29 12:02:06', 0),
(106, 'emmanuel.pelletier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$tnTtQ3Dlf7/KZpP/vDYlue6k27DUW58Hz4pFPJ2uDNWYnlFYzhcGK', 'Emmanuel', 'Pelletier', '012m.jpg', '2021-10-29 12:02:07', '2021-10-29 12:02:07', '2021-10-29 12:02:07', 0),
(107, 'edith.ferreira@gmail.com', '[\"ROLE_USER\"]', '$2y$13$iKYkPKtlInF62cbof0qeGuwRGaDAbOl4fl.ydm6yG53Pd8WbcoPOy', 'Édith', 'Ferreira', '013f.jpg', '2021-10-29 12:02:07', '2021-10-29 12:02:07', '2021-10-29 12:02:07', 0),
(108, 'noemi.moreno@gmail.com', '[\"ROLE_USER\"]', '$2y$13$kCG3IqnNZF9frQNVzSTgfughY88L1Jz6Nq2PYnZ3amTaHWZtOu8cC', 'Noémi', 'Moreno', '014f.jpg', '2021-10-29 12:02:08', '2021-10-29 12:02:08', '2021-10-29 12:02:08', 0),
(109, 'marcelle.laurent@gmail.com', '[\"ROLE_USER\"]', '$2y$13$y625F5Ifct24JqRfKur6BeFm2kj5ukmk.zTWHiaeKphFNQl.3Y75G', 'Marcelle', 'Laurent', '015f.jpg', '2021-10-29 12:02:08', '2021-10-29 12:02:08', '2021-10-29 12:02:08', 0),
(110, 'margot.gosselin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$OjmDa.KAT.YZIZ.5FWSys.gxOYyX8djS6ncaTSVghX1jwK9Zqv5dm', 'Margot', 'Gosselin', '016f.jpg', '2021-10-29 12:02:09', '2021-10-29 12:02:09', '2021-10-29 12:02:09', 0),
(111, 'alix.pichon@gmail.com', '[\"ROLE_USER\"]', '$2y$13$WqsTBClNSMtunQM9mCNvku.mb4yOF/c6kDj3aF04mGgOItkyx.ayS', 'Alix', 'Pichon', '017f.jpg', '2021-10-29 12:02:09', '2021-10-29 12:02:09', '2021-10-29 12:02:09', 0),
(112, 'elisabeth.chretien@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Mjgr7v2u/OyipgStjhN/r.Rz0CDre0m/pd/TajdzL.H/cR7GlT2ou', 'Élisabeth', 'Chretien', '018f.jpg', '2021-10-29 12:02:10', '2021-10-29 12:02:10', '2021-10-29 12:02:10', 1),
(113, 'marguerite.albert@gmail.com', '[\"ROLE_USER\"]', '$2y$13$.quD1RqJSZL7UBMnzqWO3eERy6wRQ04l1VA2U25Mux3Dv8lOIGGh6', 'Marguerite', 'Albert', '019f.jpg', '2021-10-29 12:02:10', '2021-10-29 12:02:10', '2021-10-29 12:02:10', 1),
(114, 'marcel.pichon@gmail.com', '[\"ROLE_USER\"]', '$2y$13$tyf4N7zlhGQ5ZI56H.6/sedj7PhZVWEAkOGC8pQmnHwPhEOMvieom', 'Marcel', 'Pichon', '020m.jpg', '2021-10-29 12:02:11', '2021-10-29 12:02:11', '2021-10-29 12:02:11', 0),
(115, 'zoe.blanc@gmail.com', '[\"ROLE_USER\"]', '$2y$13$vEhRal0A6rtrnfxqaJOVpeB5VQ.ZDl7ZX9LeEf18tCVWt/r6O7U2C', 'Zoé', 'Blanc', '021f.jpg', '2021-10-29 12:02:11', '2021-10-29 12:02:11', '2021-10-29 12:02:11', 0),
(116, 'martine.germain@gmail.com', '[\"ROLE_USER\"]', '$2y$13$XnHxftEdU8H4yOwFoEOOLunNU2lGF9KNuSO3kv1RukQxjH.pW2FIG', 'Martine', 'Germain', '022f.jpg', '2021-10-29 12:02:12', '2021-10-29 12:02:12', '2021-10-29 12:02:12', 1),
(117, 'stephane.noel@gmail.com', '[\"ROLE_USER\"]', '$2y$13$PNx9SHxb9vNJSVl6JZCTx.37VtCGQLqUq/vwM.YA.6eEnltJHhDBi', 'Stéphane', 'Noel', '023m.jpg', '2021-10-29 12:02:13', '2021-10-29 12:02:13', '2021-10-29 12:02:13', 0),
(118, 'edith.gay@gmail.com', '[\"ROLE_USER\"]', '$2y$13$a8D/HpLLETaoSJ3iX.E.mOCKb5NvLMxGF4b.UHe3EWs04V93fk4hi', 'Édith', 'Gay', '024f.jpg', '2021-10-29 12:02:13', '2021-10-29 12:02:13', '2021-10-29 12:02:13', 0),
(119, 'hugues.fleury@gmail.com', '[\"ROLE_USER\"]', '$2y$13$/oV6.ItsgwSOUdk2w9HkK.X0rqA3WdP6wYXUkysbEo1cyKKtPuVbW', 'Hugues', 'Fleury', '025m.jpg', '2021-10-29 12:02:14', '2021-10-29 12:02:14', '2021-10-29 12:02:14', 0),
(120, 'matthieu.benoit@gmail.com', '[\"ROLE_USER\"]', '$2y$13$yUmfTfUeJxQASVKmfsJlGeAG4IcSFXihsGiQ4tXPcI/pPL5Lxkfsq', 'Matthieu', 'Benoit', '026m.jpg', '2021-10-29 12:02:14', '2021-10-29 12:02:14', '2021-10-29 12:02:14', 0),
(121, 'benjamin.prevost@gmail.com', '[\"ROLE_USER\"]', '$2y$13$n.qNIIVh4hrXhZR30GjjOuE6MGTBdFkr8Sh3mHCA.WZgJH6LbiSSG', 'Benjamin', 'Prevost', '027m.jpg', '2021-10-29 12:02:15', '2021-10-29 12:02:15', '2021-10-29 12:02:15', 0),
(122, 'henri.bernier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$CSuHRdJUQrOAgeJfMmqUcefEU5Qv57KvU7u0bKu87FZEnXbKfnLvK', 'Henri', 'Bernier', '028m.jpg', '2021-10-29 12:02:15', '2021-10-29 12:02:15', '2021-10-29 12:02:15', 0),
(123, 'philippine.vallee@gmail.com', '[\"ROLE_USER\"]', '$2y$13$jy7DSrOirSNosECZsyMJHeVB3wmAxDrgj9hWiZXWJ9nVCYSIdWsBS', 'Philippine', 'Vallee', '029f.jpg', '2021-10-29 12:02:16', '2021-10-29 12:02:16', '2021-10-29 12:02:16', 0),
(124, 'francoise.berthelot@gmail.com', '[\"ROLE_USER\"]', '$2y$13$hYeInZ2yUd4SOFzrHNUenunA.F8vlO2MnJEsFquaPLOfwxMLrGGLe', 'Françoise', 'Berthelot', '030f.jpg', '2021-10-29 12:02:17', '2021-10-29 12:02:17', '2021-10-29 12:02:17', 0),
(125, 'cecile.carpentier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$oiZpVcrA6KboGXlE3B9/eeuwyD2.nnjX/Yd/L5oWm/B4k8SXYu.vS', 'Cécile', 'Carpentier', '031f.jpg', '2021-10-29 12:02:17', '2021-10-29 12:02:17', '2021-10-29 12:02:17', 0),
(126, 'antoinette.payet@gmail.com', '[\"ROLE_USER\"]', '$2y$13$bcfrP.LJhe4/CoXH1ZcR7O8OQMnphYixNcS9wR0lYyZPDEfK6p//.', 'Antoinette', 'Payet', '032f.jpg', '2021-10-29 12:02:18', '2021-10-29 12:02:18', '2021-10-29 12:02:18', 0),
(127, 'luc.dias@gmail.com', '[\"ROLE_USER\"]', '$2y$13$/36rQAx//zwmvfB3h1w2YOV9Jljma/fWfMT5TPscozbo2u7K5vJ7S', 'Luc', 'Dias', '033m.jpg', '2021-10-29 12:02:18', '2021-10-29 12:02:18', '2021-10-29 12:02:18', 0),
(128, 'luc.devaux@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Z6/d/vJYDr0uIpgyGHT7cuhw2y7cOFJafCTWOiL5uB1cnLs9.9w3.', 'Luc', 'Devaux', '034m.jpg', '2021-10-29 12:02:19', '2021-10-29 12:02:19', '2021-10-29 12:02:19', 0),
(129, 'emile.girard@gmail.com', '[\"ROLE_USER\"]', '$2y$13$SItKz3MWnEVh.f.y9cO.wOKu.Xg9KSx.rrYDEM6pFzPmHEueHbU/G', 'Émile', 'Girard', '035m.jpg', '2021-10-29 12:02:19', '2021-10-29 12:02:19', '2021-10-29 12:02:19', 0),
(130, 'honore.david@gmail.com', '[\"ROLE_USER\"]', '$2y$13$qdMRgzUwTvPSy4zwyBubwudR7sNRMoy56CEWEcNPdJJ6wUAlhihD2', 'Honoré', 'David', '036m.jpg', '2021-10-29 12:02:20', '2021-10-29 12:02:20', '2021-10-29 12:02:20', 1),
(131, 'remy.fabre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$V.J5Dc5d9Ix6rQKvXqH9mOSMjjpzN1U8NL.l8g/JDv2c3dhK72vKe', 'Rémy', 'Fabre', '037m.jpg', '2021-10-29 12:02:20', '2021-10-29 12:02:20', '2021-10-29 12:02:20', 0),
(132, 'renee.lombard@gmail.com', '[\"ROLE_USER\"]', '$2y$13$OfRZ1cEIxQ1mlEd8swLcO.E4VfhD2cQYCaBtKcuugmT8ZjvKO6uOq', 'Renée', 'Lombard', '038f.jpg', '2021-10-29 12:02:21', '2021-10-29 12:02:21', '2021-10-29 12:02:21', 0),
(133, 'margot.guerin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$qAgpPcbFhrxorYW2hRf6NugaSJ36CFOrR5rVwaT0q2ay4JhHmwhx.', 'Margot', 'Guerin', '039f.jpg', '2021-10-29 12:02:22', '2021-10-29 12:02:22', '2021-10-29 12:02:22', 0),
(134, 'oceane.hoarau@gmail.com', '[\"ROLE_USER\"]', '$2y$13$AHpU9qMc/zS/7adO0Wcp8ehI4aHgvoyC7CZrVhouolYuqeB9dWzre', 'Océane', 'Hoarau', '040f.jpg', '2021-10-29 12:02:22', '2021-10-29 12:02:22', '2021-10-29 12:02:22', 0),
(135, 'marc.gregoire@gmail.com', '[\"ROLE_USER\"]', '$2y$13$QNmifuBdvgwncwLPOnTF3eLRkj0tLTNyBcWJUWN2L4MjdGYoLar8K', 'Marc', 'Gregoire', '041m.jpg', '2021-10-29 12:02:23', '2021-10-29 12:02:23', '2021-10-29 12:02:23', 0),
(136, 'madeleine.sauvage@gmail.com', '[\"ROLE_USER\"]', '$2y$13$uZrePgrO9NdWI0h0POpE1O3mYISqMV0h6XNqkqLd.Qk42WNgPpf42', 'Madeleine', 'Sauvage', '042f.jpg', '2021-10-29 12:02:23', '2021-10-29 12:02:23', '2021-10-29 12:02:23', 0),
(137, 'philippe.buisson@gmail.com', '[\"ROLE_USER\"]', '$2y$13$cF3YY7L4z/1FosurYV1irOCC.BAUaPGNHfjGc7/1tNdstIokt50hq', 'Philippe', 'Buisson', '043m.jpg', '2021-10-29 12:02:24', '2021-10-29 12:02:24', '2021-10-29 12:02:24', 0),
(138, 'paul.gay@gmail.com', '[\"ROLE_USER\"]', '$2y$13$vpuWAYuGFkPumqUMoQrmRuKWX9QcLgSmvCyMQpoy63WQgzf6OUJXy', 'Paul', 'Gay', '044m.jpg', '2021-10-29 12:02:24', '2021-10-29 12:02:24', '2021-10-29 12:02:24', 0),
(139, 'thibault.rossi@gmail.com', '[\"ROLE_USER\"]', '$2y$13$Gkda4yUp.k60DWt63FhtjOVgIt11kge7p1jqH3rrK1jqvjMR7SjjS', 'Thibault', 'Rossi', '045m.jpg', '2021-10-29 12:02:25', '2021-10-29 12:02:25', '2021-10-29 12:02:25', 0),
(140, 'constance.cousin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$lzE16w6hi8mImk1fDU9E7ue9qk1JukjUQtSXoamRrBi5b4gjxyl1e', 'Constance', 'Cousin', '046f.jpg', '2021-10-29 12:02:25', '2021-10-29 12:02:25', '2021-10-29 12:02:25', 0),
(141, 'astrid.laurent@gmail.com', '[\"ROLE_USER\"]', '$2y$13$6d7rYVif7E7a4F4lNxeysuY8QOL0EdciazIzVq5vcYb3wGcQhVYWy', 'Astrid', 'Laurent', '047f.jpg', '2021-10-29 12:02:26', '2021-10-29 12:02:26', '2021-10-29 12:02:26', 0),
(142, 'bernard.guerin@gmail.com', '[\"ROLE_USER\"]', '$2y$13$HRKucQEvWxSOPDb8Nyv1euFb.mTQ5m6so7WrUSTr0JFa1utXfL2cC', 'Bernard', 'Guerin', '048m.jpg', '2021-10-29 12:02:27', '2021-10-29 12:02:27', '2021-10-29 12:02:27', 0),
(143, 'aurelie.boulanger@gmail.com', '[\"ROLE_USER\"]', '$2y$13$PwmDYkAO8NN.Ain2H24xsO9wd5ZI/X9.tYk4riwUiGeM/xM.6J4NC', 'Aurélie', 'Boulanger', '049f.jpg', '2021-10-29 12:02:27', '2021-10-29 12:02:27', '2021-10-29 12:02:27', 1),
(144, 'oceane.sanchez@gmail.com', '[\"ROLE_USER\"]', '$2y$13$dHD8q0ASKQCay0Lq90SwcOjmpILHvb1nFy.yx0mvU2fpQygOzv/Ve', 'Océane', 'Sanchez', '050f.jpg', '2021-10-29 12:02:28', '2021-10-29 12:02:28', '2021-10-29 12:02:28', 0),
(145, 'martin.dos-santos@gmail.com', '[\"ROLE_USER\"]', '$2y$13$gwVsogGMBRDbnnGK3M778.Gpf2CMMmenjgW/PmKD6trhR.reO3Hf2', 'Martin', 'Dos Santos', '051m.jpg', '2021-10-29 12:02:28', '2021-10-29 12:02:28', '2021-10-29 12:02:28', 0),
(146, 'madeleine.gay@gmail.com', '[\"ROLE_USER\"]', '$2y$13$lehJKD43mA0UE6ZXCXJ5a.eggVQUyHUGlU2coSvSK62pkxuzEN4aS', 'Madeleine', 'Gay', '052f.jpg', '2021-10-29 12:02:29', '2021-10-29 12:02:29', '2021-10-29 12:02:29', 0),
(147, 'alexandria.alexandre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$DNOUFoMReiTh62XnxQOCK.1Zw4rC2avs8Nf7H0rqR.loG.02eftgS', 'Alexandria', 'Alexandre', '053f.jpg', '2021-10-29 12:02:29', '2021-10-29 12:02:29', '2021-10-29 12:02:29', 0),
(148, 'eric.faivre@gmail.com', '[\"ROLE_USER\"]', '$2y$13$r6Kn7rppps2ow1m00K42peGsCi1fB4w4drEzM/qaci4ZS6D5CZMTS', 'Éric', 'Faivre', '054m.jpg', '2021-10-29 12:02:30', '2021-10-29 12:02:30', '2021-10-29 12:02:30', 0),
(149, 'gerard.lebreton@gmail.com', '[\"ROLE_USER\"]', '$2y$13$SLu63toDCHAZQahyfBeOFOCzYqVXL/yY6DU8s0lxSl7BIAdMMYq9O', 'Gérard', 'Lebreton', '055m.jpg', '2021-10-29 12:02:30', '2021-10-29 12:02:30', '2021-10-29 12:02:30', 0),
(150, 'victor.pasquier@gmail.com', '[\"ROLE_USER\"]', '$2y$13$69GsW4IqZzAVKqP/8uTeCeBoKggYRBd1kCZPgXQkJmqorQcGXQ432', 'Victor', 'Pasquier', '056m.jpg', '2021-10-29 12:02:31', '2021-10-29 12:02:31', '2021-10-29 12:02:31', 0),
(151, 'emile.deschamps@gmail.com', '[\"ROLE_USER\"]', '$2y$13$KucpdcX6SxG6D6EoZNVdveSQpv219p6Fyk6pETLbm4pUo26B9w6cG', 'Émile', 'Deschamps', '057m.jpg', '2021-10-29 12:02:32', '2021-10-29 12:02:32', '2021-10-29 12:02:32', 0),
(152, 'susan.marchal@gmail.com', '[\"ROLE_USER\"]', '$2y$13$6BHGEz3kgHdegyuuaAWZ5.a5msGWiP/4BSdHUZOkkcQ7xv.BqSfRC', 'Susan', 'Marchal', '058f.jpg', '2021-10-29 12:02:32', '2021-10-29 12:02:32', '2021-10-29 12:02:32', 0),
(153, 'augustin.bousquet@gmail.com', '[\"ROLE_USER\"]', '$2y$13$ajm6qzV1s77ysYLXriPhtOFlv7s3lY8KdKXoofphDWFAQGnCzI54O', 'Augustin', 'Bousquet', '059m.jpg', '2021-10-29 12:02:33', '2021-10-29 12:02:33', '2021-10-29 12:02:33', 0),
(154, 'maurice.lagarde@gmail.com', '[\"ROLE_USER\"]', '$2y$13$28bYb.Wi8qcJqou8QTiWTuwkcko3rT1//sYh9QicOaRd23epUy7aG', 'Maurice', 'Lagarde', '060m.jpg', '2021-10-29 12:02:33', '2021-10-29 12:02:33', '2021-10-29 12:02:33', 0),
(155, 'john.doe@gmail.com', '[\"ROLE_ADMIN\"]', '$2y$13$U9JVVWq2EpEPkATY8QeojeBPXcCA9owSrKo9huEZnY7jmg0/REAxm', 'John', 'Doe', '075m.jpg', '2021-10-29 12:02:34', '2021-10-29 12:02:34', '2021-10-29 12:02:34', 0),
(156, 'pat.mar@gmail.com', '[\"ROLE_SUPER_ADMIN\"]', '$2y$13$IeQKRJfQm24.sekFYVVRd.F.ThXzSeHD5MshJaLw4JN3Y3d7WO.hq', 'Pat', 'Mar', '074m.jpg', '2021-10-29 12:02:34', '2021-10-29 12:02:34', '2021-10-29 12:02:34', 0);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526C591CC992` FOREIGN KEY (`course_id`) REFERENCES `course` (`id`),
  ADD CONSTRAINT `FK_9474526CF675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);

--
-- Contraintes pour la table `course`
--
ALTER TABLE `course`
  ADD CONSTRAINT `FK_169E6FB912469DE2` FOREIGN KEY (`category_id`) REFERENCES `course_category` (`id`),
  ADD CONSTRAINT `FK_169E6FB95FB14BA7` FOREIGN KEY (`level_id`) REFERENCES `course_level` (`id`);

--
-- Contraintes pour la table `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `FK_1DD39950F675F31B` FOREIGN KEY (`author_id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
